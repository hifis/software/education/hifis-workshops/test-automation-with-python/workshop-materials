<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# Test Automation with Python

Learn to test your Python code effectively.

## Curriculum

Here you find the curriculum of this workshop which is laid out for two full workshop days.
The timings might vary depending on the interests of the workshop participants.
If you want to set up a workshop based on this materials, please see the [Workshop Setup Information](setup/).

1. [Introduction to test automation](episodes/episode01.md)
1. [Introduction to pytest](episodes/episode02.md)
1. [HTML parser: Introduction and initial characterization tests](episodes/episode03.md)
1. [HTML parser: Test isolation with test doubles](episodes/episode04.md)
1. [HTML parser: Analyzing the code coverage](episodes/episode05.md)
1. [HTML parser: Improving the test suite and the code structure](episodes/episode06.md)
1. [HTML parser: Build automation and CI/CD with GitLab](episodes/episode07.md)

### Schedule

| Time | Topic |
| ------ | ------ |
| 09:00 - 09:30 | Welcome & Introduction |
| 09:30 - 10:00 | Introduction to test automation |
| 10:00 - 11:00 | pytest basics |
| 11:00 - 12:00 | HTML parser: Introduction and initial characterization tests |
| 12:00 - 13:00 | *Lunch Break* |
| 13:00 - 14:00 | HTML parser: Test isolation with test doubles |
| 14:00 - 15:00 | HTML parser: Analyzing the code coverage |
| 15:00 - 15:45 | HTML parser: Improving the test suite and the code structure |
| 15:45 - 16:30 | HTML parser: Build automation and CI/CD with GitLab |
| 16:30 - 17:00 | QA / Wrap Up / Feedback |

*The actual schedule may vary slightly depending on the interests of the participants.*

## Contributors

Here you find the main contributors to the material:

- Tobias Schlauch <<Tobias.Schlauch@dlr.de>>
- Michael Meinel <<Michael.Meinel@dlr.de>>
- Christian Hüser <<c.hueser@hzdr.de>>
- Benjamin Wolff <<benjamin.wolff@dlr.de>>

## Contributing

Please see [the contribution guidelines](CONTRIBUTING.md) for further information about how to contribute.

## Changes

Please see the [Changelog](CHANGELOG.md) for notable changes of the material.

## License

Please see the file [LICENSE.md](LICENSE.md) for further information about how the content is licensed.
