<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
SPDX-FileCopyrightText: 2020 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC-BY-4.0
-->

# Introduction to Test Automation

Software testing is the process of executing and analyzing a software to find errors.
Errors are deviations of the actual from the required state, for example:

- The software does not calculate the product of a number series but its sum. This error is a deviation from the functional requirement.
- The software provides the result after one second and not after one millisecond as required. This error is a deviation from the quality requirement performance.

Typically, testing provides no proof of correctness but it helps to create confidence in the software by showing how well it fulfils the desired characteristics.
Test automation is an important tool to keep your software changeable and maintainable.
For more information, please see [[1](#references)].

## Why Do We Need Automated Tests?

When you change software, you typically think about the following aspects:

- What do we need to change?
- How can we make sure that the changes are correct?
- How can we make sure we do not break anything?

The last aspect becomes quite challenging quickly because you typically only change a small portion of the code.
But you have to make sure that a growing number code keeps its behavior.
The next figure (reproduced from [[2](#references)], page 7) visualizes this aspect.

![Protect Existing Behavior](images/protect-behavior.svg)

Despite careful planning, an uneasy feeling remains because it is hard to manually determine whether everything still works.
Particularly, the situation can lead to a vicious circle:

- It feels hard to change the software which leads to an **avoidance of changes**.
  Particularly, activities such as refactoring which help to improve code structure and code understandability are likely to be avoided.
- Missing refactoring activities lead to an **increased system complexity**.
- An increased system complexity makes it even harder to change the software.

Overall, productivity and system stability suffer with such an approach.

When you perform code changes supported by automated tests, you are effectively working with a safety net:

- Tests protect code behavior.
- Test execution is automated.
- Change of behavior results in instant feedback.

Overall, such an approach makes even larger modifications possible with less fear of breaking existing behavior.

In addition, you should consider the following principles:

1. Closely align programming and testing activities! -
   Otherwise you might forget to write some important tests.
1. Tests must be automated, repeatable, and fast! -
   Otherwise it is likely that you run them only once a year.
1. Make sure that your test suite is easy run! -
   Otherwise it is likely nobody will run them on a daily basis.
1. Testing is the responsibility of the entire team! -
   There is no magically test department which handles these things.
   Particularly, watch out for principle one.
1. Pragmatism over idealism! -
   Do not be too attached to the "right" way.
   Make sure it still works for everyone in practice.
1. Testing not as an end in itself! -
   The focus is still on writing great software.

## Test Types

A specific problem in context of software testing is its ambiguous terminology.
For example, a concrete test could be classified as "dynamic test", "white box test", "function test", and "unit test".
Another problem is the differentiation between the terms.
For example, what do we consider a unit test and when does it effectively become an integration tests?
These type of problems make it hard to effectively talk about testing.
In the following, we introduce a simplified terminology based on [[3](#references)], page 44 which is summed up in the next figure.

### Large Tests

> Large tests check whether the software works as a whole in a production-like environment.
> These tests involve most code units and external services.

- Typical issues:
  - Complex test setup as the software needs to be deployed into a production-like environment.
  - Long execution times as setting up the environment might take time and many components and external services are involved.
  - Test results are often unreliable as they depend on many additional factors.
- Other typically used terms: system tests, end-to-end-tests, system integration tests
- Examples:
  - Test matrices performing reference runs for a simulation tool.
  - Check of a data analysis script that it still works with the example data.
  - Check of the user login into a Web application.

![Large Test Type](images/large-test-type.svg)

### Medium Tests

> Medium tests checks the interaction of a subset of code units and external services.

- Differences to large tests:
  - The test setup is less complex.
  - The test execution time (< 1s) is faster.
  - The tests produce more reliable results.
- Other typically used terms: integration tests, functional tests
- Examples:
  - Check of the access to a data retrieval service.
  - Check of the database connection.

![Medium Test Type](images/medium-test-type.svg)

### Small Tests

> Small tests check a specific code unit in isolation.

- These tests provide extremely fast and reliable feedback.
  However, you have to make sure that you do not "isolate away" the real source of errors.
- Other typically used terms: unit tests, module tests, class tests
- Examples:
  - Check of a square sum calculation algorithm.
  - Check of a HTML link parser algorithm.

![Small Test Type](images/small-test-type.svg)

### Delimitation by Resource Usage

The following table shows which resources you can use in which test type (reproduced from [[3](#references)], page 46).

|         Resource        | Large Tests  |     Medium Tests   |     Small Tests    |
|:-----------------------:|:------------:|:------------------:|:------------------:|
|     Network             |       X      |      local host    |      mocked        |
|     Database            |       X      |          X         |      mocked        |
|     File system         |       X      |          X         |      mocked        |
|     GUI                 |       X      |     discouraged    |      mocked        |
|     System calls        |       X      |     discouraged    |          -         |
|     Multiple threads    |       X      |          X         |     discouraged    |
|     Sleep statements    |       X      |          X         |          -         |

The differentiation by resource helps to easier draw the line between the different test types.
In addition, it provides a clearer view on the environment required for their execution.
Particularly, small and medium tests should run directly on the developer machine and should not require a very special environment.

## Structuring Your Test Suite

The test pyramid provides a model to create a fast and maintainable automated test suite.
It has been originally introduced by Mike Cohn in his book *Succeeding with Agile* and is shown by the next figure (adapted from [[4](#references)]).

![Test Pyramid](images/test-pyramid.svg)

- The lowest level consists of **small tests** which focus on testing your basic algorithms and code behavior in isolation.
  These tests run extremely fast and provide reliable feedback.
  They are essential to keep your software maintainable!
- The middle level consists of **medium tests** which check the integration points of different code units as well as with external resources and services.
- The highest level consists of **large tests** which test selected aspects of the software running in a production-like environment end-to-end.

Medium and large tests ensure that the software works as a whole.
They verify that software works from the user`s point of view.

While the test pyramid provides a quite simple heuristic to structure your test suite.
You should not become too attached to concrete numbers, layers and names.
Those aspects depend on the concrete nature of your software.
However, there are two important takeaway points:

1. You should write tests with different level of granularity which complement each other.
1. The more high-level you get, the less tests you should write.

The last aspect is the core concept of the diagram.
The foundation of the test pyramid are small tests that cover small code units
and that run very fast in order to get immediate feedback while developing in 
*test driven development* cycles.
Medium and large tests are usually much slower than small tests which might
slow down your development pace drastically.
Hence, the largest proportion of your test cases should always be small tests.
In fact *slow tests* are often considered to be a bad practice or even a
so-called *"test smell"*.

Particularly, you should make sure to **not** end up with an inverted test pyramid ("ice-cream cone") which consists of slow and hard to maintain tests.

## Key Points

- Test automation is an important tool for effective and efficient software testing.
- A fast and maintainable automated test suite is key to keep your software maintainable.
- The test pyramid provides a simple model to structure your automated test suite properly.

## References

- [1] Tobias Schlauch, Michael Meinel, Carina Haupt: "DLR Software Engineering Guidelines", Version 1.0.0, August 2018. [Online]. Available: https://doi.org/10.5281/zenodo.1344612, section "Software Test"
- [2] Michael C. Feather: "Working Effectively With Legacy Code", Prentice-Hall, 2013
- [3] James Whittaker: "How Google test Software",  Addison-Wesley, 2012
- [4] Ham Vocke: "The Practical Test Pyramid", https://martinfowler.com/articles/practical-test-pyramid.html
