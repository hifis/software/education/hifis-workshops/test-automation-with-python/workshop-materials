<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# HTML Parser: Build Automation and CI/CD with GitLab

In the [last episode](episodes/episode06.md), we completed the test suite and refactored the `HtmlParser`.
Now, we are going on and develop further features.
Particularly, it is planned that new persons join the development team.
For that reason, we defined a suitable build process and automated typical steps with the help of a build script.
In addition, we set up a GitLab project and a build pipeline to improve collaboration and to make sure that for every code change all important steps are performed.
You can find further information about build automation in [[1]](#references).

We use the example code on branch [05-add-build-automation](https://gitlab.com/hifis/hifis-workshops/test-automation-with-python/html-parser/-/tree/05-add-build-automation) for the following demonstration.

## Adding a Build Script

We created a small `Makefile` to automate common tasks with the help of the tool [make](https://www.gnu.org/software/make/).
Developers can use the build script to quickly check their local changes.
In addition, the build script forms the basis to work with CI/CD systems efficiently.

Our `Makefile` can be used as follows to run all tests:

```
$ make test
mkdir -p build
poetry run pytest --junit-xml=build/tests.xml tests/
============================= test session starts =============================
platform win32 -- Python 3.6.10, pytest-6.1.1, py-1.9.0, pluggy-0.13.1
rootdir: C:\Users\<USERNAME>\html-parser, configfile: pyproject.toml
plugins: cov-2.10.1, httpserver-0.3.5
collected 16 items

tests\fetcher_test.py .....                                              [ 31%]
tests\main_test.py ....                                                  [ 56%]
tests\parser_test.py .......                                             [100%]

- generated xml file: C:\Users\<USERNAME>\html-parser\build\tests.xml -
============================= 16 passed in 4.97s ==============================
```

`make test` executes - as we have already seen - all tests.
However, some things changed:
- The directory `build` is created initially.
  It is used to store results of the build process such as the coverage report. 
- `pytest` is run via a command named `poetry`.
  [poetry](https://python-poetry.org/docs/) is Python package management tool which makes it easier to install and update Python dependencies.
- The test run results are stored in a file named `build/test.xml`.
  This file is used later by our GitLab CI/CD pipeline.

The build script helps us to standardize how tests are usually performed and reliefs everyone from hard-to-remember details.

The `audit` build target performs even more checks and is invoked as follows:

```
$ make audit
poetry run black . --check
All done! \u2728 \U0001f370 \u2728
7 files would be left unchanged.
poetry run flake8 html_parser tests
mkdir -p build
poetry run pytest --cov=html_parser --cov-fail-under=60 --cov-report=term-missing -m "not large" tests/
============================= test session starts =============================
...
---------------------------------------------------------------------
TOTAL                        66     20     12      1    65%

Required test coverage of 60% reached. Total coverage: 65.38%

====================== 12 passed, 4 deselected in 2.84s =======================
poetry run pytest --cov=html_parser --cov-fail-under=90 --cov-report=term-missing --cov-report=html tests/
============================= test session starts =============================
...
---------------------------------------------------------------------
TOTAL                        66      1     12      0    99%
Coverage HTML written to dir build/htmlcov

Required test coverage of 90% reached. Total coverage: 98.72%

============================= 16 passed in 6.58s ==============================
poetry run reuse lint
# SUMMARY

* Bad licenses:
* Deprecated licenses:
* Licenses without file extension:
* Missing licenses:
* Unused licenses:
* Used licenses: CC0-1.0, MIT
* Read errors: 0
* Files with copyright information: 16 / 16
* Files with license information: 16 / 16

Congratulations! Your project is compliant with version 3.0 of the REUSE Specification :-)
```

Developers can use the `audit` build target to easily check all relevant aspects before pushing their changes to the shared Git repository.
In detail the following checks are performed:
1. [black](https://black.readthedocs.io/en/stable/) checks for code formatting issues.
1. [flake8](https://flake8.pycqa.org/en/latest) checks for common programming mistakes performing a static code analysis.
1. `coverage.py` checks that the branch coverage of small and medium tests ist at least 60%.
   This check is a reminder that we require sufficient coverage by tests with fine-grained assertions.
   Please remember that those thresholds are context-specific and never a goal in itself.
1. `coverage.py` checks that the branch coverage of all tests ist at least 90%.
   Again this threshold is context-specific.
1. [reuse](https://reuse.readthedocs.io/en/stable) checks that we provided sufficient copyright and license information for every file in the repository.

There are even more build targets to make the development and testing tasks easier.
Please see [the example code documentation](https://gitlab.com/hifis/hifis-workshops/test-automation-with-python/html-parser/-/tree/05-add-build-automation#available-build-targets) for further information.

## Defining the GitLab CI/CD Pipeline

You typically use [merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/) to collaborate with each other in GitLab.
A merge request can be used to discuss code changes early and to make sure that they meet defined quality requirements.
For the latter aspect, we established a [GitLab CI/CD build pipeline](https://docs.gitlab.com/ee/ci/README.html) which is shown in the next figure:

![GitLab Pipeline](images/gitlab-pipeline.png)

The build pipeline makes sure that code changes still work in all relevant reference environments:
- Initially, the `Test` stage is run.
  Here the tests are executed for all relevant Python versions.
  Only if all tests succeed, the pipeline continues with the `Audit` stage.
- In the `Audit` stage, code coverage and other aspects (see `make audit` for details) are checked with one specific Python version.
  The checks are run in parallel to speed up the pipeline.
- Finally, the `Package` stage starts and creates an installable Python package.

In context of a merge request, we enabled the following features:

- Changes can only be integrated if the build pipeline succeeds.
- Test failures are directly accessible from the merge request user interface.
- The coverage report can be directly accessed from the merge request user interface.

![Mergeable Change](images/gitlab-mergable-change.png)
![Test Failure](images/gitlab-test-failure.png)

Merge requests and build pipeline help a lot when developing software with others.

## Key Points

- Build automation and a suitable build script are important to standardize testing and additional tests.
- GitLab offers a good support for collaboration and build automation via merge requests and build pipelines.

## Where to Go from Here?

There is much more to explore with regard to testing in Python:
- The [Python Testing Tools Taxonomy](https://wiki.python.org/moin/PythonTestingToolsTaxonomy) is a curated Wiki of Python testing tools.
- [Awesome Python](https://github.com/vinta/awesome-python) provides a nice curated list of different Python libraries and tools including testing and code analysis.

Particularly, [hypothesis](https://hypothesis.works/products/) is worth a closer look.
It enables [property-based testing](https://hypothesis.works/articles/what-is-property-based-testing/) in Python which allows to check invariants by generating suitable test data.
The [PYCON2018 talk "Beyond Unit Tests: Taking Your Testing to the Next Level"](https://www.hillelwayne.com/talks/beyond-unit-tests/) by Hillel Wayne provides a good introduction to property-based testing and its relation to other techniques.

## References

- [1] Tobias Schlauch, Michael Meinel, Carina Haupt: "DLR Software Engineering Guidelines", Version 1.0.0, August 2018. [Online]. Available: https://doi.org/10.5281/zenodo.1344612, section "Automation and Dependency Management"
