<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)

Text is licensed under:
SPDX-License-Identifier: CC-BY-4.0

Code snippets are licensed under:
SPDX-License-Identifier: MIT
-->

# HTML Parser: Introduction and Initial Characterization Tests

Imagine you are the new maintainer of the HTML parser tool.
The former maintainer already left and sent you the link to the Git repository.

The basic things you already know about the tool are illustrated in the next figure and can be summed up as follows:

- The command line tool is written in Python and extracts links from HTML files.
- The results are currently directly printed on the console.
- It supports the analysis of local HTML files but it can also retrieve files served by a Web server.
- In addition, the maintainer claimed that further retrieval methods can be easily added.

![HTML Parser Context View](images/html-parser-context-view.png)

There are already some new features planned.
But unfortunately, there are no tests :-(

## How to Approach Legacy Code?

As we have already learnt in [epsiode 1](episode01.md), we should aim for an automated test suite with tests that provide fast feedback.
However, it is usually hard to write small and even medium tests in such a code base.
The main problem is typically that you need to perform code changes (refactoring) before you can write these kind of tests.
For example, if you want to test the `HTMLParser` object of this code base, you have to write something like this:

```python
def test_parser_success():
    fetcher = ...
    html_parser = HTMLParser(fetcher)
    links = html_parser.extract_links()
    assert len(links) ...
```

To properly test this method with a small test, we might want to change the `HTMLParser` constructor.
But how can we do it safely without any tests in place?
For that reason, we have to apply a slightly different strategy:

- First, we create a set of medium and large tests. These tests function as the **initial safety net**.
- Then, we can more safely perform necessary code changes and create additional small tests step by step.
- Finally, we can restructure and optimize our test suite by, for example, removing no longer needed (large) tests.

The test cases of the initial safety net are also referred to as `characterization tests`.
Their purpose is to understand and keep the end to end behavior of the application under test.

## What Should Be Covered by Small Tests?

Another problem is to test a specific behavior of the application on the right test level.
The following figure (based on [[1]](#references)) summarizes the costs and benefits of small tests.

![Costs Benefits Small Tests](images/costs-benefits-small-tests.svg)

- Particularly, **algorithms** (e.g., parsing data, calculations) are well suited to be tested with small tests.
  This code is usually quite complex (e.g., it contains many loops and conditionals) but it is already fairly self-contained.
- **Coordinator code** is typically quite trivial but glues different modules and orchestrates their execution.
  In such a case, it is often better to use a medium test instead because it helps you to see problems when integrating the different modules.
- **Trivial code** (e.g., simple getter or setter methods) is not worth to be tested explicitly by a small tests.
  Usually, they are already covered by a medium or large test.
- In case of **overcomplicated code**, it is usually too expensive to write small tests directly.
  In such a case it is recommended to apply the strategy above: Cover the code with large tests and refactor it into algorithms, coordinator or trivial code.

When looking at the HTML parser code, we can see the following:

- The `main.py` and `fetcher.py` modules mainly orchestrate the execution of other modules.
  I.e., they are examples of **coordinator code**.
- The `parser.py` module is a bit too coupled to the `fetcher.py` module and could require some refactoring.
  I.e., it can be considered as an example of **overcomplicated code**.

But before we start changing the parser code, we write some characterization tests.

## Creating Initial Characterization Tests

In the following, we use the example code on branch [01-add-further-characterization-tests](https://gitlab.com/hifis/hifis-workshops/test-automation-with-python/html-parser/-/tree/01-add-further-characterization-tests).
After you have obtained the example code, please open a terminal and navigate to right directory.

Then, we have to set the `PYTHONPATH` environment variable to make sure that the `html_parser` package can be found by the Python interpreter.

- You can set the environment variable in the `bash shell` as follows: `export PYTHONPATH=.`
- You can set the environment variable in the `cmd.exe` (Windows default command-line interpreter) as follows: `set PYTHONPATH=.`

As long as you do not close your terminal, this setting should also work for the following code examples.

Now, we want to run the HTML parser tool as follows:

```
python -m html_parser.main
General Usage:        main.py [PATH TO HTML DOCUMENT]
It currently parses a HTML document and prints all links.

Example local file:   main.py /home/user/test.html
Example HTTP:         main.py https://www.google.de/
```

We see a help message that shows us how we can use the tool.

In the next step, we want to parse the DLR Web site as follows:

```
python -m html_parser.main https://www.dlr.de
...
http://filmarchivvopr.dlr.de/de
content/de/artikel/news/2020/03/20200925_40-jahre-deutsch-chinesische-kooperation-in-der-luftfahrtforschung.html
content/de/artikel/news/2020/03/20200902_startschuss-fuer-aerospacepark-am-dlr-trauen.html
content/de/artikel/news/2020/03/20200730_besuch-der-bundestagsabgeordneten-klein-und-koehler-im-dlr.html
content/de/artikel/news/2020/03/20200722_mdb-andreas-laemmel-zu-gast-beim-dlr-inbremen.html
content/de/artikel/news/2020/03/20200708_bundesagentur-fuer-sprunginnovationen-im-dlr-op.html
content/de/artikel/news/2020/02/20200630_wasserstoff-energiequelle-fuer-die-luftfahrt.html
content/de/artikel/news/2020/02/20200526_dlr-zeigt-flagge-fuer-vielfalt.html
SiteGlobals/Forms/Suche/Alle-Nachrichten/DE/alle-nachrichten_formular.html?cl2Categories_Themenbereiche=a8654c63-a8bf-4d0e-9dff-5f78a9164c26&pageLocale=de
SiteGlobals/Forms/Suche/Alle-Nachrichten/DE/alle-nachrichten_formular.html?cl2Categories_Themenbereiche=a8654c63-a8bf-4d0e-9dff-5f78a9164c26&pageLocale=de
```

There are a lot of links contained as you can see.

In the next step, we want to run the available characterization tests with the help of `pytest` as follows:

```
python -m pytest tests
============================= test session starts =============================
platform win32 -- Python 3.6.10, pytest-6.1.1, py-1.9.0, pluggy-0.13.1
rootdir: C:\Users\<USERNAME>\html-parser
plugins: cov-2.10.1, httpserver-0.3.5
collected 2 items

tests\main_test.py ..                                                    [100%]

============================== 2 passed in 0.51s ===============================
```

There seems to be a test module named `main_test.py` which contains two test cases.
Both of them run successfully.

The test cases look as follows:

```python
def test_parse_localfile_success(tmpdir, capfd):
    # Create a valid HTML document
    html_document = tmpdir.join("test.html")
    html_document.write("<html><body><a href='new_link.html'>Test</a></body></html>")

    # Run the command
    exit_code = _run_main_script([str(html_document)])

    # Make sure it is successfully run
    assert exit_code == 0
    assert capfd.readouterr()[0].startswith("new_link.html")


def test_parse_remotefile_success(capfd):
    pass
```

The test case `test_parse_localfile_success` is structured as follows:
1. A local temporary HTML file is created with the help of the pytest test fixture `tmpdir`.
1. The HTML parser tool is run using a helper function.
   This function actually invokes the Python interpreter and HTML parser tool in a subprocess.
  I.e., this test case can be considered as a large test.
1. It is checked that the exit code of the script indicates no problems and that the right link has been printed on the command-line.
   For that purpose, we use the `capfd` fixture which can access the standard output stream.

Finally, you can see an empty test case named `test_parse_remotefile_success` that we want to implement next.

### Exercise

- Please implement the `test_parse_remotefile_success` test case.
  The test should retrieve a HTML file from a Web server (e.g., `https://www.dlr.de`) and check for a specific link in the command-line output.
  - Please make sure to provide the right parameter to the `_run_main_script()` function.
  - Please use the `capfd` fixture to check the output of the command-line tool.
    You can check whether a string is contained in the output as follows: `assert "link" in capfd.readouterr()[0]`
- Please run the tests again to make sure that everything works as expected.
  - What changes do you notice when running the tests?
  - How stable do you consider this test case?

## Key Points

- When testing legacy code without any tests, you usually need to establish a safety net consisting of **characterization tests**.
- These tests are usually large tests which can help you to safely refactor overcomplicated code into testable units.
- It is important to test a specific functionality on the right test level and to keep an eye on the overall test suite structure.
- `pytest` can be used to automate medium and large tests as well.

## References

- [1] Steve Sanderson: "Selective Unit Testing – Costs and Benefits", http://blog.stevensanderson.com/2009/11/04/selective-unit-testing-costs-and-benefits/
