<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)

Text is licensed under:
SPDX-License-Identifier: CC-BY-4.0

Code snippets are licensed under:
SPDX-License-Identifier: MIT
-->

# HTML Parser: Improving the Test Suite and the Code Structure

In the [last episode](episode05.md), we analyzed the code coverage of the HTML parser tool.
On this basis, we added a test case to cover an edge case in the parser.
In addition, we realized that the error handling in `main.py` and `fetcher.py` is not tested at all.
For that purpose, we added further error handling test cases to `main_test.py` and added a new test module to properly test the `fetcher.py` module.
 
Now, we are ready to refactor the `HtmlParser` towards a cleaner design.

## Refactor the Parser Module

At the moment, we have to create a `Fetcher` instance and hand it over to the `HtmlParser` constructor.
Then, the parser can retrieve the HTML content with the help of the `Fetcher` instance.
But would not it be better to directly inject the HTML content into the parser?

Below you can find a summary of the performed changes.
After the refactoring, the `main.py` module handles content retrieval and the `parser.py` module receives the HTML content directly. 

<table>
<tr><th></th><th>Before</th><th>After</th></tr>

<tr>
<td>main.py</td>
<td>

```python
fetcher = create_fetcher(path)
parser = HtmlParser(fetcher)
try:
    extracted_links = parser.extract_links()
except HtmlParserError as error:
    ...
```

</td>
<td>

```python
try:
    fetcher = create_fetcher(path)
    html_document = fetcher.retrieve()
    parser = HtmlParser(html_document)
    extracted_links = parser.extract_links()
except (FetcherError, HtmlParserError) as error:
    ...
```

</td>
</tr>
<tr>
<td>parser.py</td>
<td>

```python
def __init__(self, fetcher):
    self._fetcher = fetcher

def extract_links(self):
    try:
        html_document = self._fetcher.retrieve()
    except FetcherError as error:
        raise HtmlParserError("Cannot retrieve content.")
    else:
        parser = bs4.BeautifulSoup(html_document, "html.parser")
        ...
```

</td>
<td>

```python
def __init__(self, html_document):
    self._html_document = html_document

def extract_links(self):
    parser = bs4.BeautifulSoup(self._html_document, "html.parser")
    ...        
```

</td>
</tr>
</table>

As another consequence, the test cases for the `HtmlParser` become really easy to write and to understand because we no longer require stub objects!

```python
def test_extract_links_success(self):
    parser = HtmlParser("<a href='/index.html'>index.html</a>")

    extracted_links = parser.extract_links()

    assert len(extracted_links) == 1
    assert extracted_links[0] == "/index.html"
```

Overall, every change could be quickly checked because we had a fast test suite in place.

## Mark the Different Test Types

We want to explicitly mark medium and large tests with the help of the `pytest marker feature`.
On this basis, we can easily select the right tests, for example, to run only the fast small tests.
In the following, we use the example code on branch [04-mark-test-types](https://gitlab.com/hifis/hifis-workshops/test-automation-with-python/html-parser/-/tree/04-mark-test-types).

Initially, we create a `pytest.ini` file as follows:

```
[pytest]
markers =
    medium: marks slower medium tests (deselect with '-m "not medium"')
    large: marks slower large tests (deselect with '-m "not large"')
```

We define markers for medium and large tests.
Test cases without theses markers are considered as small tests.

In the test code, we can use the markers as follows:

```python
@pytest.mark.large
def test_parse_localfile_success(tmpdir, capfd):
    ...
```

Here we mark the test case `test_parse_localfile_success` explicitly as a large test.

Now, we can use the markers to select the test cases that we want to run.
For example, we can run all large tests as follows:

```
python -m pytest -m "large" tests
============================= test session starts =============================
platform win32 -- Python 3.6.10, pytest-6.1.1, py-1.9.0, pluggy-0.13.1
rootdir: C:\Users\<USERNAME>\html-parser, configfile: pytest.ini
plugins: cov-2.10.1, httpserver-0.3.5
collected 16 items / 12 deselected / 4 selected

tests\main_test.py ....                                                  [100%]

====================== 4 passed, 12 deselected in 2.94s =======================
```

We can see that only the four large tests have been selected and executed.

In addition, we can also combine the markers logically, for example, to only run all small tests as follows:

```
python -m pytest -m "not large and not medium" tests
============================= test session starts =============================
platform win32 -- Python 3.6.10, pytest-6.1.1, py-1.9.0, pluggy-0.13.1
rootdir: C:\Users\<USERNAME>\html-parser, configfile: pytest.ini
plugins: cov-2.10.1, httpserver-0.3.5
collected 16 items / 9 deselected / 7 selected

tests\parser_test.py .......                                             [100%]

======================= 7 passed, 9 deselected in 0.23s =======================
```

Now, only the seven parser tests are executed.

### Analyzing the Structure of the Test Suite (Live Demonstration)

In the following, we consider the different test types and determine their number of tests, their execution time and their achieved code coverage.
The tests have been executed on the following system: Windows 10 (64bit), i7-8650U CPU @ 1.90 GHz, 16 GB RAM.

#### Execute **all** tests

```bash
python -m pytest --cov=html_parser --cov-branch tests
```

- Number of tests: 16
- Execution time 5.2s
- Coverage: 97%

#### Execute **only large** tests:

```bash
python -m pytest -m "large" --cov=html_parser --cov-branch tests
```

- Number of tests: 4
- Execution time 3.1s
- Coverage: 93%

#### Execute **only medium** tests:

```bash
 python -m pytest -m "medium" --cov=html_parser --cov-branch tests
```

- Number of tests: 5
- Execution time 2.8s
- Coverage: 38%


#### Execute **only small** tests:

```bash
python -m pytest -m "not large and not medium" --cov=html_parser --cov-branch tests
```

- Number of tests: 7
- Execution time 0.25s
- Coverage: 41%

#### Execute **small and medium** tests:

```bash
python -m pytest -m "not large" --cov=html_parser --cov-branch tests
```

- Number of tests: 12
- Execution time 2.8s
- Coverage: 55%

#### Summary

- Even our coordinator code focused application fits well with the test pyramid concept (7 small tests, 5 medium tests, 4 large tests).
  Only the `parser.py` module is well suited for writing small tests.
- Execution times:
  - The large tests took the longest execution time (0.78s per test case).
  - The small tests are really fast (0.04s per test case)!
- Code coverage:
  - Large tests produce a lot of code coverage (93%) with only 4 test cases.
    But this coverage lacks proper assertions.
    I.e., it is still quite hard to find the reason for the detected error or subtle errors are not found at all.
  - Medium and small tests produce a good portion of code coverage (55%).
    Particularly, they allow fine-grained assertions.
- Overall, the test suite looks fine.
  In future, we expect a growing number of small tests.
  Particularly, for new features of the `parser.py` module.

## Key Points

- A safety net consisting of automated tests helps a lot to perform refactoring activities safely.
- `pytest markers` make it easy to select the tests that you want to execute.
- It is important that you have the right mix of tests in your test suite.
