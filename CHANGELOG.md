<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.0.0] - 2020-11-09

### Added

- Initial version of the workshop material


[unreleased]: https://gitlab.com/hifis/hifis-workshops/test-automation-with-python/workshop-materials/compare/1.0.0...master
[1.0.0]: https://gitlab.com/hifis/hifis-workshops/test-automation-with-python/workshop-materials/-/tags/1.0.0

