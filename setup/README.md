<!--
SPDX-FileCopyrightText: 2021 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

## Workshop Setup

Here you find basic information about the workshop setup.

## Plan the Schedule

We use the following schedule for an online setup of the workshop.
Please modify times and dates according to your needs.

## Day 1 - (DD.MM.YYYY)

- 09:00 - 09:30 Welcome & Introduction
- 09:30 - 10:00 Introduction to test automation
- 10:00 - 12:15 Introduction to pytest
- 12:15 - 13:00 HTML parser: Introduction and initial characterization tests 
- 13:00 - 13:05 Wrap up day 1

## Day 2 - (DD.MM.YYYY)

- 09:00 - 09:15 Recap day 1
- 09:15 - 10:45 HTML parser: Test isolation with test doubles
- 10:45 - 11:15 HTML parser: Analyzing the code coverage
- 11:15 - 11:45 HTML parser: Improving the test suite and the code structure
- 11:45 - 12:15 HTML parser: Build automation and CI/CD with GitLab
- 12:15 - 13:00 QA / Wrap Up / Feedback


## Setup the Pads

The directory [pads](pads) contains pad templates that we usually set up in a service such as [HedgeDoc](https://hedgedoc.org/):
- Throughout the whole workshop, we use the [Main Pad](./setup/pads/main.md).
- Please copy and modify the pad to your needs
- Content to be changed is indicated by `[setup]`
